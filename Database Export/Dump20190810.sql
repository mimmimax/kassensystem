-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: kassensystem
-- ------------------------------------------------------
-- Server version	5.5.62-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `archive`
--

DROP TABLE IF EXISTS `archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive` (
  `archiveID` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL,
  `retailprice` float DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`archiveID`),
  UNIQUE KEY `archive_archiveID_uindex` (`archiveID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive`
--

LOCK TABLES `archive` WRITE;
/*!40000 ALTER TABLE `archive` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemarchive`
--

DROP TABLE IF EXISTS `itemarchive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemarchive` (
  `archiveID` int(11) NOT NULL AUTO_INCREMENT,
  `itemID` int(11) DEFAULT NULL,
  `name` char(30) NOT NULL,
  `retailprice` double DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`archiveID`),
  UNIQUE KEY `archive_archiveID_uindex` (`archiveID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemarchive`
--

LOCK TABLES `itemarchive` WRITE;
/*!40000 ALTER TABLE `itemarchive` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemarchive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemdeliveries`
--

DROP TABLE IF EXISTS `itemdeliveries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemdeliveries` (
  `itemdeliveryID` int(11) NOT NULL AUTO_INCREMENT,
  `itemID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`itemdeliveryID`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemdeliveries`
--

LOCK TABLES `itemdeliveries` WRITE;
/*!40000 ALTER TABLE `itemdeliveries` DISABLE KEYS */;
INSERT INTO `itemdeliveries` VALUES (1,1,20),(2,2,120),(3,3,35),(4,2,50),(5,3,26),(6,4,50),(7,5,162),(8,6,57),(9,6,54),(10,7,109),(11,8,109),(12,9,58),(13,10,161),(14,11,58),(15,12,109),(16,13,109),(17,14,47),(18,15,109),(19,16,33),(20,17,10),(21,18,161),(22,19,161),(23,20,161),(24,20,9),(25,21,47),(26,22,47),(29,16,20),(30,17,5),(31,17,10),(32,1,50),(33,1,-3),(34,24,30),(35,20,5),(36,20,5),(37,20,10),(38,25,50),(39,17,-24),(41,24,-100),(42,24,100),(44,24,-100),(45,24,-10),(46,24,85),(47,24,46),(71,17,50),(73,23,15),(80,23,-9),(81,26,50),(82,27,30),(83,28,1),(84,26,-55),(85,26,5),(86,26,20),(87,29,50),(88,22,50),(91,22,-95),(92,22,20),(93,30,23);
/*!40000 ALTER TABLE `itemdeliveries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `itemID` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL,
  `retailprice` float DEFAULT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `category` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'Brot',5,1,1),(2,'Cola',2.5,1,0),(3,'Burger',4.5,0,1),(4,'Käseauflauf',7.6,0,1),(5,'Cola',2.5,0,0),(6,'VeggieBurger',6.5,0,1),(7,'VeggieBurger',6.5,0,1),(8,'VeggieBurger',6.5,0,1),(9,'Burger',4.5,0,1),(10,'Cola',2.5,0,0),(11,'Burger',4.5,1,1),(12,'VeggieBurger',6.5,0,1),(13,'Burger',6.5,0,1),(14,'Auflauf',7.6,0,1),(15,'V-Burger',6.5,0,1),(16,'Pizza',7.99,1,1),(17,'Fanta',2.5,1,0),(18,'Cola',2.5,0,0),(19,'Cola',2.5,0,0),(20,'ColaZero',2.5,0,0),(21,'Käse-Auflauf',7.6,0,1),(22,'Käse-Auflauf',7.99,1,1),(23,'Calzone',8.5,1,1),(24,'Pizza Veggie',9.8,1,1),(25,'Nudelsuppe mit Rindfleisch',11,1,1),(26,'Marmorkuchen',3.33,0,1),(27,'Sushi',9.99,1,1),(28,'test',1,0,1),(29,'Currywust',2.5,1,1),(30,'Marmorkuchen',5,1,1);
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logindata`
--

DROP TABLE IF EXISTS `logindata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logindata` (
  `waiterID` int(11) NOT NULL,
  `loginname` char(20) NOT NULL,
  `passwordhash` char(200) NOT NULL,
  UNIQUE KEY `logindata_waiterID_uindex` (`waiterID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logindata`
--

LOCK TABLES `logindata` WRITE;
/*!40000 ALTER TABLE `logindata` DISABLE KEYS */;
INSERT INTO `logindata` VALUES (1,'test','3556498'),(2,'marie.maier','3003444'),(8,'bernd.quer','3556498'),(9,'harald.brecht','3556498');
/*!40000 ALTER TABLE `logindata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderarchive`
--

DROP TABLE IF EXISTS `orderarchive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderarchive` (
  `archiveID` int(11) NOT NULL AUTO_INCREMENT,
  `orderID` int(11) DEFAULT NULL,
  `ordertime` datetime DEFAULT NULL,
  `tableID` int(11) DEFAULT NULL,
  `waiterID` int(11) DEFAULT NULL,
  `archivetime` datetime DEFAULT NULL,
  PRIMARY KEY (`archiveID`),
  UNIQUE KEY `archiveID_UNIQUE` (`archiveID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderarchive`
--

LOCK TABLES `orderarchive` WRITE;
/*!40000 ALTER TABLE `orderarchive` DISABLE KEYS */;
INSERT INTO `orderarchive` VALUES (1,6,'2017-12-03 19:51:12',1,2,'2019-08-10 13:31:50'),(2,10,'2017-12-14 13:07:19',2,3,'2019-08-10 13:31:50'),(3,11,'2017-12-14 13:35:39',4,4,'2019-08-10 13:31:50'),(4,38,'2018-03-09 11:04:36',1,1,'2019-08-10 13:31:50'),(5,48,'2018-03-15 11:41:26',5,2,'2019-08-10 13:31:50'),(6,65,'2018-03-15 12:36:08',5,4,'2019-08-10 13:31:50'),(7,73,'2018-03-15 17:35:50',5,1,'2019-08-10 13:31:50'),(8,74,'2018-03-15 17:38:08',5,1,'2019-08-10 13:31:50'),(9,75,'2018-03-15 17:40:17',2,2,'2019-08-10 13:31:50'),(10,76,'2018-03-15 17:42:06',2,2,'2019-08-10 13:31:50'),(11,77,'2018-03-15 17:44:50',2,2,'2019-08-10 13:31:50'),(12,79,'2018-03-18 16:39:47',2,1,'2019-08-10 13:31:50'),(13,85,'2018-03-18 16:50:58',2,1,'2019-08-10 13:31:50'),(14,93,'2018-03-18 17:00:57',2,1,'2019-08-10 13:31:50'),(15,96,'2018-03-18 17:01:38',7,1,'2019-08-10 13:31:50'),(16,99,'2018-03-18 17:11:40',5,1,'2019-08-10 13:31:50'),(17,100,'2018-03-18 17:15:14',1,1,'2019-08-10 13:31:50'),(18,103,'2018-03-18 17:19:44',7,1,'2019-08-10 13:31:50'),(19,109,'2018-03-19 12:50:45',1,4,'2019-08-10 13:31:50'),(20,110,'2018-03-19 12:51:14',1,4,'2019-08-10 13:31:50'),(21,114,'2018-03-19 12:59:57',1,4,'2019-08-10 13:31:50'),(22,117,'2018-03-19 13:05:36',4,4,'2019-08-10 13:31:50'),(23,118,'2018-03-19 13:07:44',4,4,'2019-08-10 13:31:50'),(24,119,'2018-03-19 13:08:12',5,4,'2019-08-10 13:31:50');
/*!40000 ALTER TABLE `orderarchive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordereditemarchive`
--

DROP TABLE IF EXISTS `ordereditemarchive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordereditemarchive` (
  `archiveID` int(11) NOT NULL AUTO_INCREMENT,
  `orderID` int(11) DEFAULT NULL,
  `itemID` int(11) DEFAULT NULL,
  PRIMARY KEY (`archiveID`),
  UNIQUE KEY `archiveID_UNIQUE` (`archiveID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordereditemarchive`
--

LOCK TABLES `ordereditemarchive` WRITE;
/*!40000 ALTER TABLE `ordereditemarchive` DISABLE KEYS */;
INSERT INTO `ordereditemarchive` VALUES (1,6,6),(2,10,16),(3,11,11),(4,38,1),(5,48,1),(6,65,1),(7,73,1),(8,74,1),(9,75,1),(10,76,1),(11,77,1),(12,79,24),(13,85,20),(14,93,16),(15,96,1),(16,99,23),(17,100,1),(18,103,22),(19,109,1),(20,110,16),(21,114,25),(22,117,1),(23,118,1);
/*!40000 ALTER TABLE `ordereditemarchive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordereditems`
--

DROP TABLE IF EXISTS `ordereditems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordereditems` (
  `orderedItemID` int(11) NOT NULL AUTO_INCREMENT,
  `orderID` int(11) NOT NULL,
  `itemID` int(11) NOT NULL,
  `itemPaid` tinyint(1) DEFAULT '0',
  `itemProduced` tinyint(1) DEFAULT '0',
  `comment` char(30) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `firstUserID` int(11) DEFAULT NULL,
  `secondUserID` int(11) DEFAULT NULL,
  `thirdUserID` int(11) DEFAULT NULL,
  `fourthUserID` int(11) DEFAULT NULL,
  `firstscan` char(30) DEFAULT NULL,
  `secondscan` char(30) DEFAULT NULL,
  `thirdscan` char(30) DEFAULT NULL,
  `fourthscan` char(30) DEFAULT NULL,
  PRIMARY KEY (`orderedItemID`)
) ENGINE=InnoDB AUTO_INCREMENT=612 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordereditems`
--

LOCK TABLES `ordereditems` WRITE;
/*!40000 ALTER TABLE `ordereditems` DISABLE KEYS */;
INSERT INTO `ordereditems` VALUES (611,207,26,0,0,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ordereditems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `orderID` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `tableID` int(11) NOT NULL,
  `waiterID` int(11) DEFAULT NULL,
  PRIMARY KEY (`orderID`)
) ENGINE=InnoDB AUTO_INCREMENT=208 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (207,'2019-08-10 13:40:29',7,1);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statustext`
--

DROP TABLE IF EXISTS `statustext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statustext` (
  `statusID` int(11) NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`statusID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statustext`
--

LOCK TABLES `statustext` WRITE;
/*!40000 ALTER TABLE `statustext` DISABLE KEYS */;
INSERT INTO `statustext` VALUES (0,'Die Bestellung wurde angelegt'),(1,'Die Bearbeitung der Bestellung wurde begonnen'),(2,'Das Produkt ist fertig'),(3,'Das Produkt befindet sich in der Auslieferung'),(4,'Das Produkt wurde ausgeliefert');
/*!40000 ALTER TABLE `statustext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statustexttustext`
--

DROP TABLE IF EXISTS `statustexttustext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statustexttustext` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statustexttustext`
--

LOCK TABLES `statustexttustext` WRITE;
/*!40000 ALTER TABLE `statustexttustext` DISABLE KEYS */;
/*!40000 ALTER TABLE `statustexttustext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tables`
--

DROP TABLE IF EXISTS `tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tables` (
  `tableID` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(30) NOT NULL,
  `seats` int(11) DEFAULT '0',
  `available` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`tableID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tables`
--

LOCK TABLES `tables` WRITE;
/*!40000 ALTER TABLE `tables` DISABLE KEYS */;
INSERT INTO `tables` VALUES (1,'A1',5,1),(2,'A2',4,1),(3,'A3',10,0),(4,'B1',2,1),(5,'B2',2,1),(6,'B3',4,0),(7,'B66',6,1),(8,'B36',8,0),(9,'B33',8,1),(10,'A4',5,1),(11,'B6',10,1),(12,'G5',3,1),(13,'G1',8,1),(14,'B36-a',8,1);
/*!40000 ALTER TABLE `tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `waiters`
--

DROP TABLE IF EXISTS `waiters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `waiters` (
  `waiterID` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` char(20) DEFAULT NULL,
  `prename` char(20) DEFAULT NULL,
  `employed` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`waiterID`),
  UNIQUE KEY `waiters_waiterID_uindex` (`waiterID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `waiters`
--

LOCK TABLES `waiters` WRITE;
/*!40000 ALTER TABLE `waiters` DISABLE KEYS */;
INSERT INTO `waiters` VALUES (1,'Müller','Hans',1),(2,'Maier','Max',1),(3,'Hohenhaus','Leila',1),(4,'Paulus','Oliver',0),(7,'Jodel','Jan',0),(8,'Quer','Bernd',1),(9,'Brecht','Harald',1),(10,'Drang','Daniel Detlef',1);
/*!40000 ALTER TABLE `waiters` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-10 13:48:55
