package dhbw.sa.kassensystemapplication.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.joda.deser.DateTimeDeserializer;
import com.fasterxml.jackson.datatype.joda.ser.DateTimeSerializer;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Model für einen Datensatz der Datenbanktabelle orderedItem.
 *
 * @author Daniel Schifano
 */
public class OrderedItem {
    @JsonProperty private int orderedItemID;
    @JsonProperty private int orderID;
    @JsonProperty private int itemID;
    @JsonProperty private boolean itemPaid;
    @JsonProperty private boolean itemProduced;
    @JsonProperty private String comment;
    @JsonProperty private int status;
    @JsonProperty private int waiterId;
    @JsonProperty("date") private String lastscan;

    public OrderedItem() {}

    /**
     * Konstruktor zum Abrufen eines vollständigen OrderedItems aus der Datenbank.
     * @param orderedItemID
     * @param orderID
     * @param itemID
     * @param itemPaid
     * @param itemProduced
     * @param comment
     * @param status
     * @param waiterId
     * @param lastscan
     */
    @JsonCreator
    public OrderedItem(@JsonProperty("orderedItemID")int orderedItemID,
                       @JsonProperty("orderID") int orderID,
                       @JsonProperty("itemID") int itemID,
                       @JsonProperty("itemPaid") boolean itemPaid,
                       @JsonProperty("itemProduced") boolean itemProduced,
                       @JsonProperty("comment") String comment,
                       @JsonProperty("status") int status,
                       @JsonProperty("waiter") int waiterId,
                       @JsonProperty("date") String lastscan) {
        this.orderedItemID = orderedItemID;
        this.orderID = orderID;
        this.itemID = itemID;
        this.itemPaid = itemPaid;
        this.itemProduced = itemProduced;
        this.comment = comment;
        this.status = status;
        this.waiterId = waiterId;
        this.lastscan = lastscan;
    }

    /**
     * Konstruktor zum Hinzufügen eines neuen OrderedItems in die Datenbank.
     * @param orderID
     * @param itemID
     */
    public OrderedItem(int orderID, int itemID) {
        this.orderID = orderID;
        this.itemID = itemID;
        this.itemPaid = false;
        this.itemProduced = false;
        this.waiterId = 0;
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd kk:mm:ss");
        this.lastscan = fmt.print(DateTime.now());
    }

    public int getOrderedItemID() {
        return orderedItemID;
    }

    public int getOrderID() {
        return orderID;
    }

    public int getItemID() {
        return itemID;
    }

    public boolean isItemPaid() {
        return itemPaid;
    }

    public boolean isItemProduced()
    {
        return itemProduced;
    }

    public int getStatus() {return status; }

    public int getWaiter() {return waiterId; }

    public String getDate() {return lastscan; }

    public void itemIsPaid() {
        this.itemPaid = true;
    }

    public void setItemIsProduced(boolean itemIsProduced )
    {
        this.itemProduced = itemIsProduced;
    }

    public void setItemIsPaid(boolean itemIsPaid){
        this.itemPaid = itemIsPaid;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setStatus (int status) {this.status = status;}

    public void setwaiterId (int waiterId) {this.waiterId = waiterId;}

    public void setlastscan (String date) {this.lastscan = date;}
}