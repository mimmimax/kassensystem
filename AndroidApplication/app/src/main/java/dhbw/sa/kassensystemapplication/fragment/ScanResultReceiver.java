package dhbw.sa.kassensystemapplication.fragment;

public interface ScanResultReceiver {
    /**
     * function to receive scanresult
     * @param content Ergebnis des Scans */

    public void scanResultData(String content);

    public void scanResultData(Exception noScanData);
}