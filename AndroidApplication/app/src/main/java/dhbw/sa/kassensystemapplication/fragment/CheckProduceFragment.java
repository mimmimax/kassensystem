package dhbw.sa.kassensystemapplication.fragment;


import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import dhbw.sa.kassensystemapplication.Entity;
import dhbw.sa.kassensystemapplication.MainActivity;
import dhbw.sa.kassensystemapplication.R;
import dhbw.sa.kassensystemapplication.entity.Item;
import dhbw.sa.kassensystemapplication.entity.Order;
import dhbw.sa.kassensystemapplication.entity.OrderedItem;
import dhbw.sa.kassensystemapplication.entity.Table;

/**
 * In dieser Klasse wird der Bestellungsstatus-Bildschirm der Applikation erstellt.
 *
 * @author Daniel Schifano
 */
public class CheckProduceFragment extends Fragment {

    /**
     * Nodes, in denen die Informationen für den Anwendern dargestellt werden, beziehungsweise die
     * sie verwenden können.
     */
    private Button confirmProduced;
    private TextView failurIfNoUnproducedItem;
    /**
     * Variablen, die zu "Berechnungen" innerhalb der Java-Klasse verwendet werden.
     */
    public static String text = null;
    private int sizeOfRelativeLayout = 0;
    private String comment;
    boolean checked;
    /**
     * Gibt an, wie lang der String maximal sein darf, bevor eine neue Zeile angefangen werden muss.
     */
    private int lengthOfStringTillSplit1 = 17;
    /**
     * Gibt an, wie lang der String maximal sein darf, bevor eine dritte Zeile angefangen werden muss.
     */
    private int lengthOfStringTillSplit2 = 2*lengthOfStringTillSplit1;
    /**
     * Der Konstruktor, der zum aufrufen dieser Klasse benötigt wird.
     * Er benötigt keine Übergabe Parameter.
     * Damit wird der neue Bildschirm initalisiert und kann auf dem Smartphone angezeigt werden.
     */
    public CheckProduceFragment() {
        // Required empty public constructor
    }
    /**
     * Diese Methode wird aufgerufen wenn das Fragment erstellt wird. Dabei werden alle Nodes
     * initialisiert.
     * Wenn Artikel als "angenommen" markiert sind, dann werden Sie an den Server geschickt und in
     * der Datenbank gespeichert.
     *
     * @param inflater Instantiiert ein XML-Layout in ein passendes View Objekt
     * @param container Erlaubt den Zugriff auf container Eigenschaften
     * @param savedInstanceState Gibt an in welchem Abschnitt des Lebenszyklus die App sich befindet.
     *                          Ob sie z.B. geschlossen wurde oder gestartet wurde.
     * @return View die dargestellt werden soll
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_check_produce, container, false);

        failurIfNoUnproducedItem = v.findViewById(R.id.failurIfNoUnproducedItem);

        // declare the universal pixels
        final int pix = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, this.getResources().getDisplayMetrics());
        float posY = pix;
        checked = false;

        // declare the relative Layout. There the Nodes for the Order get added.
        RelativeLayout rl = (RelativeLayout) v.findViewById(R.id.rlCheckProduce);
        ViewGroup.LayoutParams params = rl.getLayoutParams();

        for (OrderedItem orderedItem : MainActivity.allunproducedItems) {

            sizeOfRelativeLayout++;
        }

        params.height = sizeOfRelativeLayout * 5 * pix;

        for (int i = 0; i < MainActivity.allunproducedItems.size(); i++) {

            final OrderedItem orderedItem = MainActivity.allunproducedItems.get(i);
            String name = null;

            //get the name from the orderedItem
            for (Item item : MainActivity.allItems) {
                if (item.getItemID() == orderedItem.getItemID()) {
                    name = item.getName();
                    break;
                }
            }
            /*if (name.length() > lengthOfStringTillSplit1) {
                name = name.substring(0, lengthOfStringTillSplit1) + "-\n" + name.substring(lengthOfStringTillSplit1);
                if (name.length() > lengthOfStringTillSplit2) {
                    name = name.substring(0, lengthOfStringTillSplit1) + "-\n" + name.substring(lengthOfStringTillSplit1, lengthOfStringTillSplit2) + "-\n" + name.substring(lengthOfStringTillSplit1);
                }
            }*/
            Table table = findTableToOrder(orderedItem.getOrderID());
            String statusText = "";
            String waiterName = "";

            switch (orderedItem.getStatus())
            {
                case 0:
                    statusText = "Bestellung wurde angelegt";
                    break;
                case 1:
                    statusText = "Bearbeitung der Bestellung wurde begonnen";
                    break;
                case 2:
                    statusText = "Produkt ist fertig";
                    break;
                case 3:
                    statusText = "Produkt befindet sich in der Auslieferung";
                    break;
                case 4:
                    statusText = "Produkt wurde ausgeliefert";
                    break;
            }

            try{
                GetWaiterName waitertask = new GetWaiterName(orderedItem.getWaiter());
                waitertask.execute();
                waiterName = waitertask.get();
            }
            catch(Exception e){
                e.printStackTrace();
            }


            text = name + " (Tisch: " + table.getName() + ", Status: " + statusText + ")";
                final TextView nameTextView = new TextView(getActivity());
                nameTextView.setLayoutParams(new LinearLayout.LayoutParams(30 * pix, 20 * pix));
                nameTextView.setText(text);
                nameTextView.setId(-i);
                nameTextView.setX(pix * 2);
                nameTextView.setY(posY);
                nameTextView.setPadding(pix, pix, pix, pix);
                rl.addView(nameTextView);

                posY = posY + 5 * pix;

                final int click = i;

            //DateTimeFormatter fmt = DateTimeFormat.forPattern("dd.MM.yyyy hh:mm");
            String datum = orderedItem.getDate();

            final String detail_head = name;
            final String detail = "Bestellt an Tisch " + table.getName() + "\nAktueller Status: " + statusText
                    + "\nzuletzt gescannt von " + waiterName + "\nDatum: " + datum;

            nameTextView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(detail_head);
                    builder.setMessage(detail);
                    AlertDialog alert1 = builder.create();
                    alert1.show();

                }
            });
        }


        if(MainActivity.allunproducedItems.size() == 0){
                failurIfNoUnproducedItem.setTextColor(Color.RED);
                failurIfNoUnproducedItem.setText("Es sind keine Artikel\nabholbereit.");
            } else {
                failurIfNoUnproducedItem.setText("");
            }

        return v;
    }
    /**
     * Mit dieser Methode wird der Tisch zu einer Bestellung herausgesucht.
     * Diese Methode wird immer dann aufgerufen, wenn ein Artikel auf dem Bildschirm dargestellt
     * werden soll. Der Tischname befindet sich immer in Klammern hinter dem Artikelname.
     *
     * @param orderID Die Bestellung, von der der Tisch gesucht wird.
     * @return Table, wenn ein Tisch zu der orderID gefunden wurde.
     *         null, wenn kein Tisch zu der orderID gefunden wurde. Dies ist vorallem dann der Fall,
     *         wenn in der Datenbank die orderID nicht mehr vorhanden ist.
     */
    private Table findTableToOrder(int orderID) {

        System.out.println("----------\nOrder-ID: " + orderID);

        int tableID = 0;

        for (Order order: MainActivity.allOrders){
            if(order.getOrderID() == orderID){
                tableID = order.getTable();

                System.out.println("\tTable-ID: " + tableID);
                break;
            }
        }

        for (Table table: MainActivity.allTables){
            if (table.getTableID() == tableID){
                System.out.println("\tTable-Name: " + table.getName());
                return table;
            }

        }
        if(orderID == 52)
            System.out.println("stop");

        System.out.println("\tnix gefunden");
        return null;
    }


    private class GetWaiterName extends AsyncTask<Void, Void, String> {
        /**
         * Mit dieser Methode wird das Passwort an den Server/Datenbank übertragen.
         *
         * @param params welche Datentypen die Informationen haben, die im Hintergrund bearbeitet
         *               werden sollen.
         * @return gibt null zurück, da Informationen lediglich an den Server geschickt werden.
         */
        int waiterID;
        String waiterName;

        public GetWaiterName (int waiterID){
            this.waiterID = waiterID;
            this.waiterName = "";
        }
        @Override
        protected String doInBackground(Void... params) {

            RestTemplate restTemplate = new RestTemplate();

            try {
                ResponseEntity<String> responseEntity =
                        restTemplate.exchange
                                (MainActivity.url + "/waiters/" + Integer.toString(waiterID), HttpMethod.GET,
                                        Entity.getEntity(waiterID), String.class
                                        );

                waiterName = responseEntity.getBody();
                text = null;
                return waiterName;

            } catch (HttpClientErrorException e) {
                text = e.getResponseBodyAsString();
                if (text.indexOf("Login") != -1) {
                    text = "Der Login ist Fehlgeschlagen.\nBitte melden Sie sich mit Ihren " +
                            "Zugangsdaten an";
                }
                return null;
            } catch (ResourceAccessException e) {
                text = "Es konnte keine Verbindung aufgebaut werden.";
                return null;
            } catch (Exception e) {
                text = "Ein Unbekannter Fehler ist aufgetreten";
                e.printStackTrace();
                return null;
            }
        }

    }

    private class GetStatus extends AsyncTask<Void, Void, String> {
        /**
         * Mit dieser Methode wird das Passwort an den Server/Datenbank übertragen.
         *
         * @param params welche Datentypen die Informationen haben, die im Hintergrund bearbeitet
         *               werden sollen.
         * @return gibt null zurück, da Informationen lediglich an den Server geschickt werden.
         */
        int statusID;
        String statusText;

        public GetStatus (int statusID){
            this.statusID = statusID;
            this.statusText = "Hier könnte ihr Status stehen";
        }
        @Override
        protected String doInBackground(Void... params) {

            RestTemplate restTemplate = new RestTemplate();

            try {
                ResponseEntity<String> responseEntity =
                        restTemplate.exchange
                                (MainActivity.url + "/status/" + Integer.toString(statusID), HttpMethod.GET,
                                        Entity.getEntity(statusID), String.class
                                );

                statusText = responseEntity.getBody();
                return statusText;

            } catch (HttpClientErrorException e) {
                text = e.getResponseBodyAsString();
                if (text.indexOf("Login") != -1) {
                    text = "Der Login ist Fehlgeschlagen.\nBitte melden Sie sich mit Ihren " +
                            "Zugangsdaten an";
                }
                return null;
            } catch (ResourceAccessException e) {
                text = "Es konnte keine Verbindung aufgebaut werden.";
                return null;
            } catch (Exception e) {
                text = "Ein Unbekannter Fehler ist aufgetreten";
                e.printStackTrace();
                return null;
            }
        }

    }
}
