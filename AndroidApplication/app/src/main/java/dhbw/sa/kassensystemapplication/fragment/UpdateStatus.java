package dhbw.sa.kassensystemapplication.fragment;

import android.os.AsyncTask;
import android.widget.Toast;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import dhbw.sa.kassensystemapplication.Entity;
import dhbw.sa.kassensystemapplication.MainActivity;

public class UpdateStatus extends AsyncTask<Void, Void, Void> {
    /**
     * Mit dieser Methode wird eine bereits bestehende Bestellung die mithilfe der Applikation
     * upgedated wurde an den Server übermittelt.
     *
     * @param params welche Datentypen die Informationen haben, die im Hintergrund bearbeitet
     * werden sollen.
     * @return gibt null zurück, da Informationen lediglich an den Server geschickt werden.
     */
    String id;
    String text;

    public UpdateStatus(String id) {
        this.id = id;
    }

    @Override
    protected Void doInBackground(Void... params) {

        RestTemplate restTemplate = new RestTemplate();

        try {

            ResponseEntity<Integer> responseEntity = restTemplate.exchange
                    (MainActivity.url + "/updateStatus", HttpMethod.PUT,
                            Entity.getEntity(Integer.parseInt(id)), Integer.class);
            text = null;

        } catch (HttpClientErrorException e) {
            text = e.getResponseBodyAsString();
            int error = e.getStatusCode().value();
            if (error == 418){
                text = "Status von ID " + id + " wurde nicht geändert, die Bedienung darf sich auf dem Weg der Auslieferung nicht ändern.";
            }
            if (error == 400){
                text = "Artikel mit ID " + id + " war bereits ausgeliefert. Status wird nicht weiter erhöht.";
            }
            e.printStackTrace();
            System.out.println("\n" + text + "\n--------------------------");
            return null;
        } catch (Exception e) {
            text = "undefinierter Fehler";
            e.printStackTrace();
            System.out.println("\n" + text + "\n--------------------------");
        }

        return null;
    }

    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if (text != null) {
            showToast(text);
        }

    }

    /**
     * Methode, die den übergebenen Text auf dem Smartphone darstellt.
     *
     * @param text Der Text welcher dargestellt werden soll.
     */
    private void showToast(String text) {
        if (text != null) {
            Toast.makeText(MainActivity.context, text, Toast.LENGTH_SHORT).show();
            System.out.println(text);
        }
    }
}