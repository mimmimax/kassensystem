package dhbw.datamodel;

import javafx.beans.property.SimpleStringProperty;

public class AnalysisModel {

    private final SimpleStringProperty name;

    public AnalysisModel(String name) {
        this.name = new SimpleStringProperty(name);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String itemName) {
        this.name.set(itemName);
    }


}

