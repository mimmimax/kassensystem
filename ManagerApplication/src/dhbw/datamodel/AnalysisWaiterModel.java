package dhbw.datamodel;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class AnalysisWaiterModel {

    private final SimpleStringProperty id;
    private final SimpleIntegerProperty order;
    private final SimpleIntegerProperty open;

    public AnalysisWaiterModel(String id, int order, int open) {

        this.id = new SimpleStringProperty(id);
        this.order = new SimpleIntegerProperty(order);
        this.open = new SimpleIntegerProperty(open);
    }


    public String getID() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setID(String id) {
        this.id.set(id);
    }

    public int getOrder() {
        return order.get();
    }

    public SimpleIntegerProperty orderProperty() {
        return order;
    }

    public void setOrder(int order) {
        this.order.set(order);
    }

    public int getopen() {
        return open.get();
    }

    public SimpleIntegerProperty openProperty() {
        return open;
    }

    public void setOpen(int open) {
        this.open.set(open);
    }

}
