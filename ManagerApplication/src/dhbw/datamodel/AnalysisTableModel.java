package dhbw.datamodel;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class AnalysisTableModel {

    private final SimpleStringProperty key;
    private final SimpleIntegerProperty value;

    public AnalysisTableModel(String key, int value) {

        this.key = new SimpleStringProperty(key);
        this.value = new SimpleIntegerProperty(value);
    }


    public String getKey() {
        return key.get();
    }

    public SimpleStringProperty keyProperty() {
        return key;
    }

    public void setKey(String key) {
        this.key.set(key);
    }

    public int getValue() {
        return value.get();
    }

    public SimpleIntegerProperty valueProperty() {
        return value;
    }

    public void setValue(int value) {
        this.value.set(value);
    }

}
