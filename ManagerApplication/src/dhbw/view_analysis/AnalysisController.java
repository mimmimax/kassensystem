package dhbw.view_analysis;

import dhbw.datamodel.AnalysisModel;
import dhbw.datamodel.AnalysisTableModel;
import dhbw.datamodel.AnalysisWaiterModel;
import dhbw.kassensystem_manager_view.KassensystemManagerController;
import dhbw.sa.kassensystem_rest.database.databaseservice.DatabaseService;
import dhbw.sa.kassensystem_rest.database.entity.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.joda.time.DateTime;

import java.util.*;

public class AnalysisController {

    public DatabaseService databaseService;
    public KassensystemManagerController kmc;
    public Stage window;


    public Tab todayTab;
    public Tab yesterdayTab;
    public Tab weekTab;
    public Tab monthTab;
    public Tab yearTab;


    public TableView TodayArticleTable;
    public TableView TodayTableTable;
    public TableView TodayWaiterTable;

    public TableView YesterdayArticleTable;
    public TableView YesterdayTableTable;
    public TableView YesterdayWaiterTable;

    public TableView WeekArticleTable;
    public TableView WeekTableTable;
    public TableView WeekWaiterTable;

    public TableView MonthArticleTable;
    public TableView MonthTableTable;
    public TableView MonthWaiterTable;

    public TableView YearArticleTable;
    public TableView YearTableTable;
    public TableView YearWaiterTable;


    public TableColumn todayArticleColumn;
    public TableColumn todaySellingsColumn;
    public TableColumn todayTableColumn;
    public TableColumn todayUsageColumn;
    public TableColumn todayWaiterColumn;
    public TableColumn todayOrdersColumn;
    public TableColumn todayOpenColumn;

    public TableColumn yesterdayArticleColumn;
    public TableColumn yesterdaySellingsColumn;
    public TableColumn yesterdayTableColumn;
    public TableColumn yesterdayUsageColumn;
    public TableColumn yesterdayWaiterColumn;
    public TableColumn yesterdayOrdersColumn;
    public TableColumn yesterdayOpenColumn;

    public TableColumn weekArticleColumn;
    public TableColumn weekSellingsColumn;
    public TableColumn weekTableColumn;
    public TableColumn weekUsageColumn;
    public TableColumn weekWaiterColumn;
    public TableColumn weekOrdersColumn;
    public TableColumn weekOpenColumn;

    public TableColumn monthArticleColumn;
    public TableColumn monthSellingsColumn;
    public TableColumn monthTableColumn;
    public TableColumn monthUsageColumn;
    public TableColumn monthWaiterColumn;
    public TableColumn monthOrdersColumn;
    public TableColumn monthOpenColumn;

    public TableColumn yearArticleColumn;
    public TableColumn yearSellingsColumn;
    public TableColumn yearTableColumn;
    public TableColumn yearUsageColumn;
    public TableColumn yearWaiterColumn;
    public TableColumn yearOrdersColumn;
    public TableColumn yearOpenColumn;


    public Button todaypdfBtn;
    public Button yesterdaypdfBtn;
    public Button weekpdfBtn;
    public Button monthpdfBtn;
    public Button yearpdfBtn;

    public Button updateAnalysis;


    public void initialize(DatabaseService databaseService, KassensystemManagerController kmc, Stage window) {
        this.databaseService = databaseService;
        this.kmc = kmc;
        this.window = window;

        this.fillArticles();
        this.fillTables();

        if(todayTab.isSelected()) {
            this.fillWaitersToday();
        }
        if(yesterdayTab.isSelected()) {
            this.fillWaitersYesterday();
        }
        if(weekTab.isSelected()) {
            this.fillWaitersWeek();
        }
        if(monthTab.isSelected()) {
            this.fillWaitersMonth();
        }
        if(yearTab.isSelected()) {
            this.fillWaitersYear();
        }

    }


    public void fillArticles() {

        ObservableMap<Integer, Integer> today = FXCollections.observableHashMap();
        ObservableMap<Integer, Integer> yesterday = FXCollections.observableHashMap();
        ObservableMap<Integer, Integer> week = FXCollections.observableHashMap();
        ObservableMap<Integer, Integer> month = FXCollections.observableHashMap();
        ObservableMap<Integer, Integer> year = FXCollections.observableHashMap();

        ObservableList<AnalysisTableModel> todayList = FXCollections.observableArrayList();
        ObservableList<AnalysisTableModel> yesterdayList = FXCollections.observableArrayList();
        ObservableList<AnalysisTableModel> weekList = FXCollections.observableArrayList();
        ObservableList<AnalysisTableModel> monthList = FXCollections.observableArrayList();
        ObservableList<AnalysisTableModel> yearList = FXCollections.observableArrayList();

        /***Artikel, default Wert 0 in Maps schreiben***/
        for (Item i : databaseService.getAllAvailableItems()) {
            today.put(i.getItemID(), 0);
            yesterday.put(i.getItemID(), 0);
            week.put(i.getItemID(), 0);
            month.put(i.getItemID(), 0);
            year.put(i.getItemID(), 0);
        }

        DateTime actualDate = DateTime.now();


        /***alle Bestellungen durchgehen, Artikel auslesen***/
        for (Order o : databaseService.getAllOrders()) {

            /***überprüfen ob Bestellung heute aufgegeben wurde***/
            if (actualDate.minusDays(1).isBefore(o.getDate())) {
                ArrayList<OrderedItem> items = databaseService.getOrderedItemsByOrderId(o.getOrderID());
                /***Anzahl bestellter Artikel nach Artikeln sortiert in Map speichern***/
                for (OrderedItem i : items) {
                    if (today.containsKey(i.getItemID())) {
                        today.replace(i.getItemID(), today.get(i.getItemID()) + 1);
                    }
                }
            }

            /***überprüfen ob Bestellung gestern aufgegeben wurde***/
            if (actualDate.minusDays(2).isBefore(o.getDate()) && actualDate.minusDays(1).isAfter(o.getDate())) {
                ArrayList<OrderedItem> items = databaseService.getOrderedItemsByOrderId(o.getOrderID());
                /***Anzahl bestellter Artikel nach Artikeln sortiert in Map speichern***/
                for (OrderedItem i : items) {
                    if (yesterday.containsKey(i.getItemID())) {
                        yesterday.replace(i.getItemID(), yesterday.get(i.getItemID()) + 1);
                    }
                }
            }

            /***überprüfen ob Bestellung innerhalb der letzten Woche aufgegeben wurde***/
            if (actualDate.minusWeeks(1).isBefore(o.getDate())) {
                ArrayList<OrderedItem> items = databaseService.getOrderedItemsByOrderId(o.getOrderID());
                /***Anzahl bestellter Artikel nach Artikeln sortiert in Map speichern***/
                for (OrderedItem i : items) {
                    if (week.containsKey(i.getItemID())) {
                        week.replace(i.getItemID(), week.get(i.getItemID()) + 1);
                    }
                }
            }

            /***überprüfen ob Bestellung innerhalb des letzten Monats aufgegeben wurde***/
            if (actualDate.minusMonths(1).isBefore(o.getDate())) {
                ArrayList<OrderedItem> items = databaseService.getOrderedItemsByOrderId(o.getOrderID());
                /***Anzahl bestellter Artikel nach Artikeln sortiert in Map speichern***/
                for (OrderedItem i : items) {
                    if (month.containsKey(i.getItemID())) {
                        month.replace(i.getItemID(), month.get(i.getItemID()) + 1);
                    }
                }
            }

            /***überprüfen ob Bestellung innerhalb des letzten Jahres aufgegeben wurde***/
            if (actualDate.minusYears(1).isBefore(o.getDate())) {
                ArrayList<OrderedItem> items = databaseService.getOrderedItemsByOrderId(o.getOrderID());
                /***Anzahl bestellter Artikel nach Artikeln sortiert in Map speichern***/
                for (OrderedItem i : items) {
                    if (year.containsKey(i.getItemID())) {
                        year.replace(i.getItemID(), year.get(i.getItemID()) + 1);
                    }
                }
            }

        }

        /***Werte aus Map in Listen speichern***/
        for (int article : today.keySet()) {
            String key = databaseService.getItemById(article).getName();
            int value = today.get(article);
            todayList.add(new AnalysisTableModel(key, value));
        }

        for (int article : yesterday.keySet()) {
            String key = databaseService.getItemById(article).getName();
            int value = yesterday.get(article);
            yesterdayList.add(new AnalysisTableModel(key, value));
        }

        for (int article : week.keySet()) {
            String key = databaseService.getItemById(article).getName();
            int value = week.get(article);
            weekList.add(new AnalysisTableModel(key, value));
        }

        for (int article : month.keySet()) {
            String key = databaseService.getItemById(article).getName();
            int value = month.get(article);
            monthList.add(new AnalysisTableModel(key, value));
        }

        for (int article : year.keySet()) {
            String key = databaseService.getItemById(article).getName();
            int value = year.get(article);
            yearList.add(new AnalysisTableModel(key, value));
        }


        /***Tabellen befüllen***/
        todayArticleColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, String>("key"));
        todaySellingsColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, Integer>("value"));

        TodayArticleTable.setItems(todayList);


        yesterdayArticleColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, String>("key"));
        yesterdaySellingsColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, Integer>("value"));

        YesterdayArticleTable.setItems(yesterdayList);


        weekArticleColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, String>("key"));
        weekSellingsColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, Integer>("value"));

        WeekArticleTable.setItems(weekList);


        monthArticleColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, String>("key"));
        monthSellingsColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, Integer>("value"));

        MonthArticleTable.setItems(monthList);


        yearArticleColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, String>("key"));
        yearSellingsColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, Integer>("value"));

        YearArticleTable.setItems(yearList);
    }


    public void fillTables() {

        ObservableMap<Integer, Integer> today = FXCollections.observableHashMap();
        ObservableMap<Integer, Integer> yesterday = FXCollections.observableHashMap();
        ObservableMap<Integer, Integer> week = FXCollections.observableHashMap();
        ObservableMap<Integer, Integer> month = FXCollections.observableHashMap();
        ObservableMap<Integer, Integer> year = FXCollections.observableHashMap();

        ObservableList<AnalysisTableModel> todayList = FXCollections.observableArrayList();
        ObservableList<AnalysisTableModel> yesterdayList = FXCollections.observableArrayList();
        ObservableList<AnalysisTableModel> weekList = FXCollections.observableArrayList();
        ObservableList<AnalysisTableModel> monthList = FXCollections.observableArrayList();
        ObservableList<AnalysisTableModel> yearList = FXCollections.observableArrayList();

        /***Artikel, default Wert 0 in Maps schreiben***/
        for (Table t : databaseService.getAllAvailableTables()) {
            today.put(t.getTableID(), 0);
            yesterday.put(t.getTableID(), 0);
            week.put(t.getTableID(), 0);
            month.put(t.getTableID(), 0);
            year.put(t.getTableID(), 0);
        }

        DateTime actualDate = DateTime.now();

        ArrayList<Table> tables = new ArrayList<>();

        for (Order o : databaseService.getAllOrders()) {
            tables.add(databaseService.getTableById(o.getTable()));
        }


        for (Order o : databaseService.getAllOrders()) {
            /***Tisch der Bestellung auslesen***/
            Table t = databaseService.getTableById(o.getTable());
            /***überprüfen ob Bestellung heute aufgegeben wurde***/
            if (actualDate.minusDays(1).isBefore(o.getDate())) {
                /***Tischbenutzungen hochzählen falls Tisch heute benutzt wurde***/
                if (today.containsKey(t.getTableID())) {
                    today.replace(t.getTableID(), today.get(t.getTableID()) + 1);
                }
            }

            /***überprüfen ob Bestellung gestern aufgegeben wurde***/
            if (actualDate.minusDays(2).isBefore(o.getDate()) && actualDate.minusDays(1).isAfter(o.getDate())) {
                /***Tischbenutzungen hochzählen falls Tisch gestern benutzt wurde***/
                if (yesterday.containsKey(t.getTableID())) {
                    yesterday.replace(t.getTableID(), yesterday.get(t.getTableID()) + 1);
                }
            }

            /***überprüfen ob Bestellung innerhalb der letzten Woche aufgegeben wurde***/
            if (actualDate.minusWeeks(1).isBefore(o.getDate())) {
                /***Tischbenutzungen hochzählen falls Tisch innerhalb der letzten Woche benutzt wurde***/
                if (week.containsKey(t.getTableID())) {
                    week.replace(t.getTableID(), week.get(t.getTableID()) + 1);
                }
            }

            /***überprüfen ob Bestellung innerhalb des letzten Monats aufgegeben wurde***/
            if (actualDate.minusMonths(1).isBefore(o.getDate())) {
                /***Tischbenutzungen hochzählen falls Tisch innerhalb des letzten Moants benutzt wurde***/
                if (month.containsKey(t.getTableID())) {
                    month.replace(t.getTableID(), month.get(t.getTableID()) + 1);
                }
            }

            /***überprüfen ob Bestellung innerhalb des letzten Jahres aufgegeben wurde***/
            if (actualDate.minusYears(1).isBefore(o.getDate())) {
                /***Tischbenutzungen hochzählen falls Tisch innerhalb des letzten Jahres benutzt wurde***/
                if (year.containsKey(t.getTableID())) {
                    year.replace(t.getTableID(), year.get(t.getTableID()) + 1);
                }
            }
        }


        /***Werte aus Map in Listen speichern***/
        for (int table : today.keySet()) {
            String key = databaseService.getTableById(table).getName();
            int value = today.get(table);
            todayList.add(new AnalysisTableModel(key, value));
        }

        for (int table : yesterday.keySet()) {
            String key = databaseService.getTableById(table).getName();
            int value = yesterday.get(table);
            yesterdayList.add(new AnalysisTableModel(key, value));
        }

        for (int table : week.keySet()) {
            String key = databaseService.getTableById(table).getName();
            int value = week.get(table);
            weekList.add(new AnalysisTableModel(key, value));
        }

        for (int table : month.keySet()) {
            String key = databaseService.getTableById(table).getName();
            int value = month.get(table);
            monthList.add(new AnalysisTableModel(key, value));
        }

        for (int table : year.keySet()) {
            String key = databaseService.getTableById(table).getName();
            int value = year.get(table);
            yearList.add(new AnalysisTableModel(key, value));
        }


        /***Tabellen befüllen***/
        todayTableColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, String>("key"));
        todayUsageColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, Integer>("value"));

        TodayTableTable.setItems(todayList);


        yesterdayTableColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, String>("key"));
        yesterdayUsageColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, Integer>("value"));

        YesterdayTableTable.setItems(yesterdayList);


        weekTableColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, String>("key"));
        weekUsageColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, Integer>("value"));

        WeekTableTable.setItems(weekList);


        monthTableColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, String>("key"));
        monthUsageColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, Integer>("value"));

        MonthTableTable.setItems(monthList);


        yearTableColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, String>("key"));
        yearUsageColumn.setCellValueFactory(new PropertyValueFactory<AnalysisTableModel, Integer>("value"));

        YearTableTable.setItems(yearList);
    }


    public void fillWaitersToday() {

        ObservableMap<Integer, Integer> todayOrder = FXCollections.observableHashMap();
        ObservableMap<Integer, Integer> todayOpen = FXCollections.observableHashMap();
        ObservableList<AnalysisWaiterModel> todayList = FXCollections.observableArrayList();

        for (Waiter w : databaseService.getAllWaiters()) {
            if (w.isEmployed()) {
                todayOrder.put(w.getWaiterID(), 0);
                todayOpen.put(w.getWaiterID(), 0);
            }
        }

        DateTime actualDate = DateTime.now();

        ArrayList<Waiter> waiters = new ArrayList<>();

        for (Order o : databaseService.getAllOrders()) {
            waiters.add(databaseService.getWaiterByID(o.getWaiterID()));
        }


        for (Order o : databaseService.getAllOrders()) {
            /***Tisch der Bestellung auslesen***/
            Waiter w = databaseService.getWaiterByID(o.getWaiterID());
            boolean paid = true;
            /***überprüfen ob Bestellung heute aufgegeben wurde***/
            if (actualDate.minusDays(1).isBefore(o.getDate())) {
                /***Tischbenutzungen hochzählen falls Tisch heute benutzt wurde***/
                for(OrderedItem i : databaseService.getOrderedItemsByOrderId(o.getOrderID())){
                    if(!i.isItemPaid()){
                        paid = false;
                    }
                }
                if (todayOrder.containsKey(w.getWaiterID())) {
                    todayOrder.replace(w.getWaiterID(), todayOrder.get(w.getWaiterID()) + 1);
                    if (!paid) {
                        todayOpen.replace(w.getWaiterID(), todayOpen.get(w.getWaiterID()) + 1);
                    }
                }
            }
        }


        /***Werte aus Map in Listen speichern***/
        for (int waiter : todayOrder.keySet()) {
            String id = databaseService.getWaiterByID(waiter).getPrename() + " " + databaseService.getWaiterByID(waiter).getLastname();
            int order = todayOrder.get(waiter);
            int open = todayOpen.get(waiter);
            todayList.add(new AnalysisWaiterModel(id, order, open));
        }


        /***Tabellen befüllen***/
        todayWaiterColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, String>("id"));
        todayOrdersColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, Integer>("order"));
        todayOpenColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, Integer>("open"));

        TodayWaiterTable.setItems(todayList);
    }

    public void fillWaitersYesterday() {

        ObservableMap<Integer, Integer> yesterdayOrder = FXCollections.observableHashMap();
        ObservableMap<Integer, Integer> yesterdayOpen = FXCollections.observableHashMap();
        ObservableList<AnalysisWaiterModel> yesterdayList = FXCollections.observableArrayList();

        /***Artikel, default Wert 0 in Maps schreiben***/
        for (Waiter w : databaseService.getAllWaiters()) {
            if (w.isEmployed()) {
                yesterdayOrder.put(w.getWaiterID(), 0);
                yesterdayOpen.put(w.getWaiterID(), 0);
            }
        }

        DateTime actualDate = DateTime.now();

        ArrayList<Waiter> waiters = new ArrayList<>();

        for (Order o : databaseService.getAllOrders()) {
            waiters.add(databaseService.getWaiterByID(o.getWaiterID()));
        }


        for (Order o : databaseService.getAllOrders()) {
            Waiter w = databaseService.getWaiterByID(o.getWaiterID());
            boolean paid = true;
            /***überprüfen ob Bestellung gestern aufgegeben wurde***/
            if (actualDate.minusDays(2).isBefore(o.getDate()) && actualDate.minusDays(1).isAfter(o.getDate())) {
                /***Tischbenutzungen hochzählen falls Tisch gestern benutzt wurde***/
                for(OrderedItem i : databaseService.getOrderedItemsByOrderId(o.getOrderID())){
                    if(!i.isItemPaid()){
                        paid = false;
                    }
                }
                if (yesterdayOrder.containsKey(w.getWaiterID())) {
                    yesterdayOrder.replace(w.getWaiterID(), yesterdayOrder.get(w.getWaiterID()) + 1);
                    if (!paid) {
                        yesterdayOpen.replace(w.getWaiterID(), yesterdayOpen.get(w.getWaiterID()) + 1);
                    }
                }
            }
        }


        /***Werte aus Map in Listen speichern***/
        for (int waiter : yesterdayOrder.keySet()) {
            String id = databaseService.getWaiterByID(waiter).getPrename() + " " + databaseService.getWaiterByID(waiter).getLastname();
            int order = yesterdayOrder.get(waiter);
            int open = yesterdayOpen.get(waiter);
            yesterdayList.add(new AnalysisWaiterModel(id, order, open));
        }


        /***Tabellen befüllen***/
        yesterdayWaiterColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, String>("id"));
        yesterdayOrdersColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, Integer>("order"));
        yesterdayOpenColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, Integer>("open"));

        YesterdayWaiterTable.setItems(yesterdayList);
    }


    public void fillWaitersWeek() {

        ObservableMap<Integer, Integer> weekOrder = FXCollections.observableHashMap();
        ObservableMap<Integer, Integer> weekOpen = FXCollections.observableHashMap();
        ObservableList<AnalysisWaiterModel> weekList = FXCollections.observableArrayList();


        /***Artikel, default Wert 0 in Maps schreiben***/
        for (Waiter w : databaseService.getAllWaiters()) {
            if (w.isEmployed()) {
                weekOrder.put(w.getWaiterID(), 0);
                weekOpen.put(w.getWaiterID(), 0);
            }
        }

        DateTime actualDate = DateTime.now();

        ArrayList<Waiter> waiters = new ArrayList<>();

        for (Order o : databaseService.getAllOrders()) {
            waiters.add(databaseService.getWaiterByID(o.getWaiterID()));
        }


        for (Order o : databaseService.getAllOrders()) {
            /***Tisch der Bestellung auslesen***/
            Waiter w = databaseService.getWaiterByID(o.getWaiterID());
            boolean paid = true;
            /***überprüfen ob Bestellung innerhalb der letzten Woche aufgegeben wurde***/
            if (actualDate.minusWeeks(1).isBefore(o.getDate())) {
                /***Tischbenutzungen hochzählen falls Tisch innerhalb der letzten Woche benutzt wurde***/
                for(OrderedItem i : databaseService.getOrderedItemsByOrderId(o.getOrderID())){
                    if(!i.isItemPaid()){
                        paid = false;
                    }
                }
                if (weekOrder.containsKey(w.getWaiterID())) {
                    weekOrder.replace(w.getWaiterID(), weekOrder.get(w.getWaiterID()) + 1);
                    if (!paid) {
                        weekOpen.replace(w.getWaiterID(), weekOpen.get(w.getWaiterID()) + 1);
                    }
                }
            }
        }


        /***Werte aus Map in Listen speichern***/
        for (int waiter : weekOrder.keySet()) {
            String id = databaseService.getWaiterByID(waiter).getPrename() + " " + databaseService.getWaiterByID(waiter).getLastname();
            int order = weekOrder.get(waiter);
            int open = weekOpen.get(waiter);
            weekList.add(new AnalysisWaiterModel(id, order, open));
        }


        /***Tabellen befüllen***/
        weekWaiterColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, String>("id"));
        weekOrdersColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, Integer>("order"));
        weekOpenColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, Integer>("open"));

        WeekWaiterTable.setItems(weekList);
    }


    public void fillWaitersMonth() {

        ObservableMap<Integer, Integer> monthOrder = FXCollections.observableHashMap();
        ObservableMap<Integer, Integer> monthOpen = FXCollections.observableHashMap();
        ObservableList<AnalysisWaiterModel> monthList = FXCollections.observableArrayList();

        /***Artikel, default Wert 0 in Maps schreiben***/
        for (Waiter w : databaseService.getAllWaiters()) {
            if (w.isEmployed()) {
                monthOrder.put(w.getWaiterID(), 0);
                monthOpen.put(w.getWaiterID(), 0);
            }
        }

        DateTime actualDate = DateTime.now();

        ArrayList<Waiter> waiters = new ArrayList<>();

        for (Order o : databaseService.getAllOrders()) {
            if(databaseService.getWaiterByID(o.getWaiterID()).isEmployed()) {
                waiters.add(databaseService.getWaiterByID(o.getWaiterID()));
            }
        }



        for (Order o : databaseService.getAllOrders()) {
            /***Tisch der Bestellung auslesen***/
            Waiter w = databaseService.getWaiterByID(o.getWaiterID());
            boolean paid = true;
            /***überprüfen ob Bestellung innerhalb des letzten Monats aufgegeben wurde***/
            if (actualDate.minusMonths(1).isBefore(o.getDate())) {
                /***Tischbenutzungen hochzählen falls Tisch innerhalb des letzten Monats benutzt wurde***/
                for(OrderedItem i : databaseService.getOrderedItemsByOrderId(o.getOrderID())){
                    if(!i.isItemPaid()){
                        paid = false;
                    }
                }
                if (monthOrder.containsKey(w.getWaiterID())) {
                    monthOrder.replace(w.getWaiterID(), monthOrder.get(w.getWaiterID()) + 1);
                    if (!paid) {
                        monthOpen.replace(w.getWaiterID(), monthOpen.get(w.getWaiterID()) + 1);
                    }
                }
            }
        }


        /***Werte aus Map in Listen speichern***/
        for (int waiter : monthOrder.keySet()) {
            String id = databaseService.getWaiterByID(waiter).getPrename() + " " + databaseService.getWaiterByID(waiter).getLastname();
            int order = monthOrder.get(waiter);
            int open = monthOpen.get(waiter);
            monthList.add(new AnalysisWaiterModel(id, order, open));
        }


        /***Tabellen befüllen***/
        monthWaiterColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, String>("id"));
        monthOrdersColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, Integer>("order"));
        monthOpenColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, Integer>("open"));

        MonthWaiterTable.setItems(monthList);
    }


    public void fillWaitersYear() {

        ObservableMap<Integer, Integer> yearOrder = FXCollections.observableHashMap();
        ObservableMap<Integer, Integer> yearOpen = FXCollections.observableHashMap();
        ObservableList<AnalysisWaiterModel> yearList = FXCollections.observableArrayList();

        /***Artikel, default Wert 0 in Maps schreiben***/
        for (Waiter w : databaseService.getAllWaiters()) {
            if (w.isEmployed()) {
                yearOrder.put(w.getWaiterID(), 0);
                yearOpen.put(w.getWaiterID(), 0);
            }
        }

        DateTime actualDate = DateTime.now();

        ArrayList<Waiter> waiters = new ArrayList<>();

        for (Order o : databaseService.getAllOrders()) {
            waiters.add(databaseService.getWaiterByID(o.getWaiterID()));
        }


        for (Order o : databaseService.getAllOrders()) {
            /***Tisch der Bestellung auslesen***/
            Waiter w = databaseService.getWaiterByID(o.getWaiterID());
            boolean paid = true;
            /***überprüfen ob Bestellung innerhalb des letzten Jahres aufgegeben wurde***/
            if (actualDate.minusYears(1).isBefore(o.getDate())) {
                /***Tischbenutzungen hochzählen falls Tisch innerhalb des letzten Jahres benutzt wurde***/
                for(OrderedItem i : databaseService.getOrderedItemsByOrderId(o.getOrderID())){
                    if(!i.isItemPaid()){
                        paid = false;
                    }
                }
                if (yearOrder.containsKey(w.getWaiterID())) {
                    yearOrder.replace(w.getWaiterID(), yearOrder.get(w.getWaiterID()) + 1);
                    if (!paid) {
                        yearOpen.replace(w.getWaiterID(), yearOpen.get(w.getWaiterID()) + 1);
                    }
                }
            }
        }


        /***Werte aus Map in Listen speichern***/
        for (int waiter : yearOrder.keySet()) {
            String id = databaseService.getWaiterByID(waiter).getPrename() + " " + databaseService.getWaiterByID(waiter).getLastname();
            int order = yearOrder.get(waiter);
            int open = yearOpen.get(waiter);
            yearList.add(new AnalysisWaiterModel(id, order, open));
        }


        /***Tabellen befüllen***/
        yearWaiterColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, String>("id"));
        yearOrdersColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, Integer>("order"));
        yearOpenColumn.setCellValueFactory(new PropertyValueFactory<AnalysisWaiterModel, Integer>("open"));

        YearWaiterTable.setItems(yearList);
    }


    public void createPDF() {

    }

    public void updateAnalysis(){
        fillArticles();
        fillTables();
        fillWaitersToday();
        fillWaitersYesterday();
        fillWaitersWeek();
        fillWaitersMonth();
        fillWaitersYear();
    }


}
