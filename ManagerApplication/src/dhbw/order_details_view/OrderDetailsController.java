package dhbw.order_details_view;

import dhbw.WindowCreator;
import dhbw.datamodel.OrderModel;
import dhbw.datamodel.OrderedItemModel;
import dhbw.sa.kassensystem_rest.database.databaseservice.DatabaseService;
import dhbw.sa.kassensystem_rest.database.entity.Item;
import dhbw.sa.kassensystem_rest.database.entity.OrderedItem;
import dhbw.sa.kassensystem_rest.database.entity.Waiter;
import dhbw.view_qrcode.QrCodeController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class OrderDetailsController implements Initializable
{
	DatabaseService databaseService;

	public TableView OrderedItemsTable;

	public Label orderIdLabel;
	public Label orderPriceLabel;
	public Label orderTableLabel;
	public Label orderDateLabel;
	public Label orderWaiterLabel;

	public TableColumn itemNameColumn;
	public TableColumn itemPriceColumn;
	public TableColumn itemProducedColumn;
	public TableColumn itemPaidColumn;
	public TableColumn itemCommentColumn;

	public ImageView QrCode;

	public void initialize(OrderModel order, DatabaseService databaseService)
	{
		orderIdLabel.setText(String.valueOf(order.getOrderID()));
		orderPriceLabel.setText(String.valueOf(order.getPrice()) + " €");
		orderTableLabel.setText(order.getTable());
		orderDateLabel.setText(order.getDate());
		Waiter waiter = databaseService.getWaiterByID(order.getWaiterID());
		orderWaiterLabel.setText(waiter.getWaiterID() + " - " +
				waiter.getPrename() + " " + waiter.getLastname());

		ArrayList<OrderedItem> orderedItems = databaseService.getOrderedItemsByOrderId(order.getOrderID());
		ObservableList<OrderedItemModel> orderedItemData = FXCollections.observableArrayList();
		// Collect data for OrderdItemModels shown in List
		for(OrderedItem o: orderedItems)
		{
			Item item = databaseService.getItemById(o.getItemID());
			String name = item.getName();
			double price = item.getRetailprice();
			boolean paid = o.isItemPaid();
			boolean produced = o.isItemProduced();
			String comment = o.getComment();
			int id = o.getOrderedItemID();
			orderedItemData.add(new OrderedItemModel(id, name, price, paid, produced, comment));

		}
		itemNameColumn.setCellValueFactory(new PropertyValueFactory<OrderModel, String>("name"));
		itemPriceColumn.setCellValueFactory(new PropertyValueFactory<OrderedItemModel, Double>("price"));
		itemPaidColumn.setCellValueFactory(new PropertyValueFactory<OrderedItemModel, Boolean>("paid"));
		itemProducedColumn.setCellValueFactory(new PropertyValueFactory<OrderedItemModel, Boolean>("produced"));
		itemCommentColumn.setCellValueFactory(new PropertyValueFactory<OrderedItemModel, String>("comment"));

		OrderedItemsTable.setItems(orderedItemData);

	}

	public void onMouseClicked(MouseEvent mouseEvent)
	{
		if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
			if(mouseEvent.getClickCount() == 2){
				System.out.println("Double clicked");
				this.showQR(null);
			}
		}
	}

	public void showQR(ActionEvent actionEvent)
	{
		Object orderedItem = OrderedItemsTable.getSelectionModel().getSelectedItem();

		WindowCreator windowCreator =
				new WindowCreator("/dhbw/view_qrcode/qrcode_view.fxml");

		QrCodeController qrCodeController = (QrCodeController) windowCreator.getController();

		qrCodeController.initialize((OrderedItemModel) orderedItem, databaseService);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources)
	{

	}

}
