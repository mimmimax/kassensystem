package dhbw.view_qrcode;

import dhbw.datamodel.OrderModel;
import dhbw.datamodel.OrderedItemModel;
import dhbw.sa.kassensystem_rest.database.databaseservice.DatabaseService;
import dhbw.sa.kassensystem_rest.database.entity.Item;
import dhbw.sa.kassensystem_rest.database.entity.OrderedItem;
import dhbw.sa.kassensystem_rest.database.entity.Waiter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class QrCodeController implements Initializable {

    DatabaseService databaseService;


    public ImageView QrCode;

    public void initialize(OrderedItemModel orderedItem, DatabaseService databaseService)
    {
        int orderedItemID = orderedItem.getId();

            Image image = new Image("file:" +"..\\QRcodes\\" + orderedItemID + ".png");
            QrCode.setImage(image);
        //file:C:/Dateien/Bosch/DHBW/Studienprojekt/Git/kassensystem/DatabaseSystem/
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {

    }

}
