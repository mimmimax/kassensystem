package dhbw.sa.kassensystem_rest.database.databaseservice;

import dhbw.sa.kassensystem_rest.database.entity.Item;
import dhbw.sa.kassensystem_rest.database.entity.Itemdelivery;
import dhbw.sa.kassensystem_rest.exceptions.MySQLServerConnectionException;
import org.joda.time.DateTime;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static dhbw.sa.kassensystem_rest.database.databaseservice.DatabaseService.convertJodaDateTimeToSqlTimestamp;

/**
 * Klasse mit Methoden zum Einsehen und Bearbeiten der Item-Tabelle.
 *
 * @author Marvin Mai
 */
class DBService_Item {
    static ArrayList<Item> getAllItems(Connection connection, boolean onlyAvailable) {
        ArrayList<Item> items = new ArrayList<>();

        try {
            String query = "SELECT itemID, name, retailprice, available, category " +
                    "FROM " + DatabaseProperties.getDatabase() + ".items";
            if (onlyAvailable)
                query += " WHERE available = TRUE";
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                //get each Item from DB
                int itemID = rs.getInt("itemID");
                String name = rs.getString("name");
                double retailprice = rs.getFloat("retailprice");
                retailprice = DatabaseService.round(retailprice);

                int quantity = getItemQuantity(connection, itemID);

                boolean available = rs.getBoolean("available");

                int category = rs.getInt("category");

                items.add(new Item(itemID, name, retailprice, quantity, available, category));
            }
            return items;
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
    }

    static Item getItemByID(Connection connection, int itemID) {
        try {
            String query = "SELECT itemID, name, retailprice, available, category " +
                    "FROM " + DatabaseProperties.getDatabase() + ".items " +
                    "WHERE itemID = " + itemID;
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                itemID = rs.getInt("itemID");
                String name = rs.getString("name");
                double retailprice = rs.getFloat("retailprice");
                retailprice = DatabaseService.round(retailprice);

                int quantity = getItemQuantity(connection, itemID);

                boolean available = rs.getBoolean("available");
                int category = rs.getInt("category");
                return new Item(itemID, name, retailprice, quantity, available, category);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
        return null;
    }

    static Item getItemByName(Connection connection, String name) {
        try {
            String query = "SELECT itemID, name, retailprice, available, category " +
                    "FROM " + DatabaseProperties.getDatabase() + ".items " +
                    "WHERE name = '" + name + "'";

            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                int itemID = rs.getInt("itemID");
                name = rs.getString("name");
                double retailprice = rs.getFloat("retailprice");
                retailprice = DatabaseService.round(retailprice);

                int quantity = getItemQuantity(connection, itemID);

                boolean available = rs.getBoolean("available");

                int category = rs.getInt("category");
                return new Item(itemID, name, retailprice, quantity, available, category);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
        return null;
    }

    static float getRetailprice(Connection connection, int itemID) {
        try {
            String query = "SELECT retailprice " +
                    "FROM " + DatabaseProperties.getDatabase() + ".items " +
                    "WHERE itemID = " + itemID;
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                return rs.getFloat("retailprice");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    static boolean getCategory(Connection connection, int itemID){
        try {
            String query = "SELECT category " +
                    "FROM " + DatabaseProperties.getDatabase() + ".items " +
                    "WHERE itemID = " + itemID;
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                return rs.getBoolean("category");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    static void addItem(Connection connection, Item item) {
        try {
            String query = "INSERT INTO " + DatabaseProperties.getDatabase() + ".items(itemID, name, retailprice, available, category) " +
                    "VALUES(DEFAULT, ?, ?, ?, ?)";
            PreparedStatement pst = connection.prepareStatement(query);

            pst.setString(1, item.getName());
            pst.setDouble(2, item.getRetailprice());
            pst.setInt(3, item.getCategory());
            pst.setBoolean(4, item.isAvailable());
            pst.executeUpdate();

            //ID des neu erzeugten Items ermitteln, um anschließend hierfuer einen neuen Wareneingang anzulegen
            query = "SELECT * FROM " + DatabaseProperties.getDatabase() +
                    ".items ORDER BY itemID DESC LIMIT 1";
            pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            int itemID = 0;
            while (rs.next()) {
                itemID = rs.getInt("itemID");
            }

            DBService_Itemdelivery.addItemdelivery(connection, new Itemdelivery(itemID, item.getQuantity()));

        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
    }

    static void updateItem(Connection connection, Item item, int itemID) {
        try {
            String query = "UPDATE " + DatabaseProperties.getDatabase() + ".items " +
                    "SET name = ?, retailprice = ?, available = ?, category = ? " +
                    "WHERE itemID = " + itemID;
            PreparedStatement pst = connection.prepareStatement(query);

            pst.setString(1, item.getName());
            pst.setDouble(2, item.getRetailprice());
            pst.setInt(3, item.getCategory());
            pst.setBoolean(4, item.isAvailable());
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
    }

    static boolean existsItemWithID(Connection connection, int itemID) {
        try {
            String query = "SELECT itemID from " + DatabaseProperties.getDatabase() + ".items " +
                    "WHERE itemID = " + itemID;
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            if (rs.next() && rs.getInt("itemID") != 0)
                return true;

        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
        return false;
    }

    /**
     * Ermittelt die aktuelle Verfuegbarkeit eines Items anhand der Wareneingaenge und Warenausgaenge.
     *
     * @param itemID des items, dessen Haeufigkeit ermittelt werden soll.
     * @return aktuelle Verfuegbarkeit des items.
     */
    private static int getItemQuantity(Connection connection, int itemID) {
        //Ermitteln der Wareneingänge
        int itemDeliveries = 0;

        try {
            String query = "SELECT quantity AS \"Quantity of Itemdeliveries\"" +
                    "FROM " + DatabaseProperties.getDatabase() + ".itemdeliveries " +
                    "WHERE itemID = " + itemID;
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                itemDeliveries += rs.getInt("Quantity of Itemdeliveries");
            }

        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }

        //Ermitteln der Warenausgänge
        int itemOrders = 0;

        try {
            String query = "SELECT COUNT(*) AS \"Quantity of Orders\"" +
                    "FROM " + DatabaseProperties.getDatabase() + ".ordereditems " +
                    "WHERE itemID = " + itemID;
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                itemOrders += rs.getInt("Quantity of Orders");
            }
            query = "SELECT COUNT(*) AS \"Quantity of Orderarchive\"" +
                    "FROM " + DatabaseProperties.getDatabase() + ".ordereditemarchive " +
                    "WHERE itemID = " + itemID;
            pst = connection.prepareStatement(query);
            rs = pst.executeQuery();

            while (rs.next()) {
                itemOrders += rs.getInt("Quantity of Orderarchive");

            }
        }
        catch (SQLException e) {
                e.printStackTrace();
                DatabaseService_Interface.connect();
                throw new MySQLServerConnectionException();
        }

        return itemDeliveries - itemOrders;
    }

    static void itemToArchive(Connection connection, int itemID) {
        try {
            String query = "SELECT itemID, name, retailprice " +
                    "FROM " + DatabaseProperties.getDatabase() + ".items WHERE itemID = " + itemID;
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            rs.next();
            itemID = rs.getInt("itemID");
            String name = rs.getString("name");
            double retailprice = rs.getFloat("retailprice");
            retailprice = DatabaseService.round(retailprice);

            int quantity = getItemQuantity(connection, itemID);

            Item item = new Item(itemID, name, retailprice, quantity, false, 0);

            query = "INSERT INTO " + DatabaseProperties.getDatabase() + ".itemarchive(archiveID, itemID, name, retailprice, date) " +
                    "VALUES(DEFAULT, ?, ?, ?, ?)";
            pst = connection.prepareStatement(query);

            pst.setString(2, item.getName());
            pst.setDouble(3, item.getRetailprice());
            pst.setObject(4, convertJodaDateTimeToSqlTimestamp(DateTime.now()));
            pst.setInt(1, item.getItemID());
            pst.executeUpdate();

            query = "DELETE FROM " + DatabaseProperties.getDatabase() + ".items WHERE itemID = " + itemID;
            pst = connection.prepareStatement(query);
            pst.executeUpdate();
            System.out.println("Artikel mit der ID " + item.getItemID() + " wurde ins Archiv verschoben.");
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }

    }
}
