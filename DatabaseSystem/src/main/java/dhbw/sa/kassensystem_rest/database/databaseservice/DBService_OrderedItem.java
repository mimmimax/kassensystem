package dhbw.sa.kassensystem_rest.database.databaseservice;

import dhbw.sa.kassensystem_rest.database.entity.OrderedItem;
import dhbw.sa.kassensystem_rest.database.printer.PrinterService;
import dhbw.sa.kassensystem_rest.exceptions.MySQLServerConnectionException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.sql.*;
import java.util.ArrayList;

import static dhbw.sa.kassensystem_rest.database.databaseservice.Log.logInf;

/**
 * Klasse mit Methoden zum Einsehen und Bearbeiten der OrderedItem-Tabelle.
 *
 * @author Marvin Mai
 */
public class DBService_OrderedItem {
    private static final String selectAllAttributs =
            "SELECT orderedItemID, orderID, itemID, itemPaid, itemProduced, comment, status, firstUserID, "
            + "secondUserID, thirdUserID, fourthUserID, firstScan, secondScan, thirdScan, fourthScan " +
                    "FROM " + DatabaseProperties.getDatabase() + ".ordereditems ";

    static ArrayList<OrderedItem> getAllOrderedItems(Connection connection, boolean onlyUnproduced) {
        ArrayList<OrderedItem> orderedItems = new ArrayList<>();

        try {
            String query = selectAllAttributs;
            if (onlyUnproduced)
                query += "WHERE status < 4 AND itemPaid = 0";
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                //get each orderedItem from DB
                int orderedItemID = rs.getInt("orderedItemID");
                int orderID = rs.getInt("orderID");
                int itemID = rs.getInt("itemID");
                boolean itemPaid = rs.getBoolean("itemPaid");
                boolean itemProduced = rs.getBoolean("itemProduced");
                String comment = rs.getString("comment");
                int status = rs.getInt("status");
                int waiter = rs.getInt("firstUserID");
                    if (rs.getBytes("secondUserID") != null)
                    {
                        if (rs.getBytes("thirdUserID") != null)
                        {
                            if (rs.getBytes("fourthUserID") != null)
                            {
                                waiter = rs.getInt("fourthUserID");
                            }
                            else
                            {
                                waiter = rs.getInt("thirdUserID");
                            }
                        }
                        else
                        {
                            waiter = rs.getInt("secondUserID");
                        }
                    }
                String date = rs.getString("firstScan");
                    if (rs.getBytes("secondScan") != null)
                    {
                        if (rs.getBytes("thirdScan") != null)
                        {
                            if (rs.getBytes("fourthScan") != null)
                            {
                                date = rs.getString("fourthScan");
                            }
                            else
                            {
                                date = rs.getString("thirdScan");
                            }
                        }
                        else
                        {
                            date = rs.getString("secondScan");
                        }
                    }


                orderedItems.add(
                        new OrderedItem(orderedItemID, orderID, itemID, itemPaid, itemProduced, comment, status, waiter, date)
                );
            }
            return orderedItems;
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
    }

    static ArrayList<OrderedItem> getAllUnproducedOrderedItemsByItemId(Connection connection, int itemID) {
        ArrayList<OrderedItem> orderedItems = new ArrayList<>();

        try {
            String query = selectAllAttributs;
            query += "WHERE itemID = " + itemID + " and itemProduced = FALSE";
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                //get each orderedItem from DB
                int orderedItemID = rs.getInt("orderedItemID");
                int orderID = rs.getInt("orderID");
                itemID = rs.getInt("itemID");
                boolean itemPaid = rs.getBoolean("itemPaid");
                boolean itemProduced = rs.getBoolean("itemProduced");
                String comment = rs.getString("comment");
                orderedItems.add(
                        new OrderedItem(orderedItemID, orderID, itemID, itemPaid, itemProduced, comment)
                );
            }
            return orderedItems;
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
    }

    static ArrayList<OrderedItem> getOrderedItemsByOrderId(Connection connection, int orderID) {
        ArrayList<OrderedItem> orderedItems = new ArrayList<>();

        try {
            String query = selectAllAttributs +
                    "WHERE orderID = " + orderID;
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                //get each orderedItem from DB
                int orderedItemID = rs.getInt("orderedItemID");
                orderID = rs.getInt("orderID");
                int itemID = rs.getInt("itemID");
                boolean itemPaid = rs.getBoolean("itemPaid");
                boolean itemProduced = rs.getBoolean("itemProduced");
                String comment = rs.getString("comment");
                orderedItems.add(
                        new OrderedItem(orderedItemID, orderID, itemID, itemPaid, itemProduced, comment)
                );
            }
            return orderedItems;
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
    }

    static ArrayList<OrderedItem> getOrderedItemsByItemId(Connection connection, int itemID) {
        ArrayList<OrderedItem> orderedItems = new ArrayList<>();

        try {
            String query = selectAllAttributs +
                    "WHERE itemID = " + itemID;
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                //get each orderedItem from DB
                int orderedItemID = rs.getInt("orderedItemID");
                int orderID = rs.getInt("orderID");
                itemID = rs.getInt("itemID");
                boolean itemPaid = rs.getBoolean("itemPaid");
                boolean itemProduced = rs.getBoolean("itemProduced");
                String comment = rs.getString("comment");
                orderedItems.add(
                        new OrderedItem(orderedItemID, orderID, itemID, itemPaid, itemProduced, comment)
                );
            }
            return orderedItems;
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
    }

    static OrderedItem getOrderedItemById(Connection connection, int orderedItemID) {
        try {
            String query = selectAllAttributs +
                    "WHERE orderedItemID = " + orderedItemID;
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            while (rs.next()) {
                //get orderedItem from DB
                orderedItemID = rs.getInt("orderedItemID");
                int orderID = rs.getInt("orderID");
                int itemID = rs.getInt("itemID");
                boolean itemPaid = rs.getBoolean("itemPaid");
                boolean itemProduced = rs.getBoolean("itemProduced");
                String comment = rs.getString("comment");
                int status = rs.getInt("status");
                int waiter = 0;
                DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
                String date = fmt.print(DateTime.now());
                if (rs.getBytes("fourthUserID") != null){
                    waiter = rs.getInt("fourthUserID");
                    date = rs.getString("fourthscan");
                }
                else if (rs.getBytes("thirdUserID") != null){
                    waiter = rs.getInt("thirdUserID");
                    date = rs.getString("thirdscan");
                }
                else if (rs.getBytes("secondUserID") != null){
                    waiter = rs.getInt("secondUserID");
                    date = rs.getString("secondscan");
                }
                else if (rs.getBytes("firstUserID") != null){
                    waiter = rs.getInt("firstUserID");
                    date = rs.getString("firstscan");
                }
                return new OrderedItem(orderedItemID, orderID, itemID, itemPaid, itemProduced, comment, status, waiter, date);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
        return null;
    }

    static void addOrderedItem(Connection connection, OrderedItem orderedItem) {
        try {
            String query = "INSERT INTO " + DatabaseProperties.getDatabase() +
                    ".ordereditems(orderedItemId, orderID, itemID, itemPaid, itemProduced, comment) " +
                    "VALUES(DEFAULT, ?, ?, DEFAULT, DEFAULT, ?)";
            PreparedStatement pst = connection.prepareStatement(query);

            pst.setInt(1, orderedItem.getOrderID());
            pst.setInt(2, orderedItem.getItemID());
            pst.setString(3, orderedItem.getComment());
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
        try {
            String query = "SELECT orderedItemID FROM " + DatabaseProperties.getDatabase() + ".ordereditems WHERE itemID = " + orderedItem.getItemID() + " and orderID = " + orderedItem.getOrderID();
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            int orderedItemID = 0;

            while (rs.next()) {
                orderedItemID = rs.getInt("orderedItemID");
            }
            PrinterService.writeQRCode(orderedItemID);
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
    }

    static void updateOrderedItem(Connection connection, OrderedItem orderedItem, int orderedItemID) {
        try {
            String user = null;
            String date = null;
            switch (orderedItem.getStatus()){
                case 0:
                    user = "firstUserID";
                    date = "firstScan";
                    break;
                case 1:
                    user =  "firstUserID";
                    date = "firstScan";
                    break;
                case 2:
                    user = "secondUserID";
                    date = "secondScan";
                    break;
                case 3:
                    user = "thirdUserID";
                    date = "thirdScan";
                    break;
                case 4:
                    user = "fourthUserID";
                    date = "fourthScan";
                    break;
            }
            String query = "UPDATE " + DatabaseProperties.getDatabase() + ".ordereditems " +
                    "SET orderID = ?, itemID = ?, itemPaid = ?, itemProduced = ?, comment = ?, " +
                    "status = ?," + user + " = ?," + date +" = ?" +
                    "WHERE orderedItemID = " + orderedItemID;
            PreparedStatement pst = connection.prepareStatement(query);

            pst.setInt(1, orderedItem.getOrderID());
            pst.setInt(2, orderedItem.getItemID());
            pst.setBoolean(3, orderedItem.isItemPaid());
            pst.setBoolean(4, orderedItem.isItemProduced());
            pst.setString(5, orderedItem.getComment());
            pst.setInt(6, orderedItem.getStatus());
            pst.setInt(7, orderedItem.getWaiter());
            pst.setString(8, orderedItem.getDate());

            logInf("OrderedItem mit ID " + Integer.toString(orderedItemID) + " auf Status " + orderedItem.getStatus() + " gesetzt" + " Waiter: " + orderedItem.getWaiter());

            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
    }

    static void deleteOrderedItem(Connection connection, int orderedItemID) {
        try {
            String query = "DELETE FROM " + DatabaseProperties.getDatabase() + ".ordereditems " +
                    "WHERE orderedItemID = " + orderedItemID;
            PreparedStatement pst = connection.prepareStatement(query);

            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
    }

    static void deleteOrderedItemByOrderId(Connection connection, int orderID) {
        try {
            String query = "DELETE FROM " + DatabaseProperties.getDatabase() + ".ordereditems " +
                    "WHERE orderID = " + orderID;
            PreparedStatement pst = connection.prepareStatement(query);

            pst.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
    }

    static boolean existsOrderedItemWithID(Connection connection, int orderedItemID) {
        try {
            String query = "SELECT orderedItemID from " + DatabaseProperties.getDatabase() + ".ordereditems " +
                    "WHERE orderedItemID = " + orderedItemID;
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            if (rs.next() && rs.getInt("orderedItemID") != 0)
                return true;

        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }
        return false;
    }

    static void ordereditemToArchive(Connection connection, int orderID) {
        try {
            String query = "SELECT ordereditemID, orderID, itemID " +
                    "FROM " + DatabaseProperties.getDatabase() + ".ordereditems WHERE orderID = " + orderID;
            PreparedStatement pst = connection.prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            rs.next();
            orderID = rs.getInt("orderID");
            int ordereditemID = rs.getInt("ordereditemID");
            int itemID = rs.getInt("itemID");

            OrderedItem ordered = new OrderedItem(orderID, itemID, "");

            query = "INSERT INTO " + DatabaseProperties.getDatabase() + ".ordereditemarchive(archiveID, orderID, itemID) " +
                    "VALUES(DEFAULT, ?, ?)";
            pst = connection.prepareStatement(query);

            pst.setInt(1, ordered.getOrderID());
            pst.setInt(2, ordered.getItemID());
            pst.executeUpdate();

            query = "DELETE FROM " + DatabaseProperties.getDatabase() + ".ordereditems WHERE ordereditemID = " + ordereditemID;
            pst = connection.prepareStatement(query);
            pst.executeUpdate();
            System.out.println("OrderedItem mit ID " + ordereditemID + " wurde ins Archiv verschoben.");
        } catch (SQLException e) {
            e.printStackTrace();
            DatabaseService_Interface.connect();
            throw new MySQLServerConnectionException();
        }

    }
}
