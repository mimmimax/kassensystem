package dhbw.sa.kassensystem_rest.database.printer;


import dhbw.sa.kassensystem_rest.database.Gastronomy;
import dhbw.sa.kassensystem_rest.database.databaseservice.DatabaseService;
import dhbw.sa.kassensystem_rest.database.entity.*;
import dhbw.sa.kassensystem_rest.exceptions.DataException;
import org.joda.time.DateTime;

import javax.imageio.ImageIO;
import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import java.awt.print.Printable;
import java.io.FileInputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.DecoderResult;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.datamatrix.decoder.Decoder;


/**
 * Service zum Ausdrucken einer Bestellung, die in einer PrintableReceipt gespeichert wurde.
 * <p>
 * Die folgenden Hardwarekomponenten sind für diesen Service erforderlich:
 * Modell des Druckers:
 * Epson TM-T88V MODEL M244A
 * Treiber muss im OS installiert sein
 * Download des Treibers:
 * ACHTUNG: 	Treiber wurde aktualisiert und ist anscheinend nicht mehr mit Code kompatibel!
 * Bitte alte Version des Treibers verwenden.
 * https://download.epson-biz.com/modules/pos/index.php?page=single_soft&cid=5131&pcat=3&scat=31
 *
 * @author Marvin Mai
 */

public class PrinterService {

    /* printerName: Name des Druckers, wie er im Betriebssystem angezeigt wird.*/
    private final String printerName = "EPSON TM-T88V Receipt";
    private final DatabaseService databaseService = new DatabaseService();

    byte qr[] = {0};

    // Interface zum Ausdrucken einer Order oder Receipt

    /**
     * Druckt über einen formatierten Text die Bestellung für die Küche aus.
     *
     * @param orderID      ID der zu druckenden Bestellung.
     * @param orderedItems Neu bestellte Artikel.
     */
    public void printOrder(int orderID, ArrayList<OrderedItem> orderedItems) {
        ArrayList<PrintableOrder> printableOrder = getPrintableOrder(orderID, orderedItems);

        String ids = "";

        for(int i = 0; i < printableOrder.size(); i++){
            String formattedOrderText = getFormattedOrder(printableOrder.get(i));

            for(PrintableOrderedItem o : printableOrder.get(i).getOrderedItems()){

                if (ids.length() == 0){
                    ids = String.format("%03d", databaseService.getItemByName(o.getName()).getItemID());
                }
                else {
                    ids += "-" + String.format("%03d", databaseService.getItemByName(o.getName()).getItemID());
                }

            }

            //qr = createQRCode(ids);

            printString(formattedOrderText, qr);
        }


        databaseService.disconnect();
    }

    /**
     * Druckt einen Kundenbeleg aus.
     *
     * @param orderID Die ID der zu druckenden Bestellung.
     */
    public void printReceipt(int orderID, ArrayList<OrderedItem> items) {
        PrintableReceipt printableReceipt = getPrintableReceipt(orderID, items);

        String formattedReceiptText = getFormattedReceipt(printableReceipt);

        printString(formattedReceiptText, qr);

        databaseService.disconnect();
    }

    public void printLogindata(String loginname, String password, Waiter waiter) {
        String formattedLogindata = getFormattedLogindata(loginname, password, waiter);

        printString(formattedLogindata, qr);
    }

    public void printDataConflict(ArrayList<OrderedItem> orderedItems) {
        String formattedText = getFormattedDataConflicts(getPrintableDataConflicts(orderedItems));

        printString(formattedText, qr);
    }

    // Funktionen zum Ermitteln aller zum Drucken benötigten Daten

    /**
     * Sammelt Daten für eine {@link PrintableOrder}, um einen Küchenbeleg ausdrucken zu können.
     *
     * @param orderID      Die ID der Order, für die neue bestellte Artikel zur Zubereitung in der Küche
     *                     ausgedruckt werden sollen.
     * @param orderedItems Die neu hinzugefügten bestellten Artikel.
     * @return Eine {@link PrintableOrder}, die alle auszudruckenden Informationen enthält.
     */
    private ArrayList<PrintableOrder> getPrintableOrder(int orderID, ArrayList<OrderedItem> orderedItems) {
        Order order = databaseService.getOrderById(orderID);

        // Date
        String dateString = order.getDate().toString("dd.MM.yyyy kk:mm:ss");
        // Table-Name
        String tableName = databaseService.getTableById(order.getTable()).getName();
        // Printable Order
        ArrayList<PrintableOrderedItem> printableOrderedItems = new ArrayList<>();
        ArrayList<PrintableOrderedItem> printableOrderedDrinks = new ArrayList<>();

        ArrayList<PrintableOrder> printableOrders = new ArrayList<>();
        for (OrderedItem o : orderedItems) {
            // Alle noch nicht in der DB existierenden OrderedItems den auszudruckenden OrderedItems hinzufügen.
            // Nur diese sollen an die Küche ausgedruckt werden.
            if (!databaseService.existsOrderedItemWithID(o.getOrderedItemID())) {
                String name = databaseService.getItemById(o.getItemID()).getName();
                printableOrderedItems.clear();
                if(databaseService.getItemById(o.getItemID()).getCategory() == 1) {
                    printableOrderedItems.add(new PrintableOrderedItem(name, o.getComment()));
                    PrintableOrder printableOrder = new PrintableOrder(dateString, tableName, (ArrayList<PrintableOrderedItem>) printableOrderedItems.clone());
                    printableOrders.add(printableOrder.clone());  //Wenn printableOrder verändert wird verändern sich auch alle Listeneinträge:(

                }else if(databaseService.getItemById(o.getItemID()).getCategory() == 0){
                    printableOrderedDrinks.add(new PrintableOrderedItem(name, o.getComment()));
                }

            }
        }
        if(!printableOrderedDrinks.isEmpty()) {
            printableOrders.add(new PrintableOrder(dateString, tableName, printableOrderedDrinks));
        }
        return printableOrders;


    }

    /**
     * Sammelt Daten für eine {@link PrintableReceipt}, um einen Kundenbeleg ausdrucken zu können.
     *
     * @param orderID Die ID der zu druckenden Bestellung.
     * @return Eine {@link PrintableReceipt}, die alle auszudruckenden Informationen enthält.
     */
    private PrintableReceipt getPrintableReceipt(int orderID, ArrayList<OrderedItem> items) {
        Order order = databaseService.getOrderById(orderID);

        // Date
        String dateString = order.getDate().toString("dd.MM.yyyy kk:mm:ss");
        double price = 0;
        // Table-Name
        String tableName = databaseService.getTableById(order.getTable()).getName();
        // PrintableOrderedItems
        ArrayList<PrintableOrderedItem> printableOrderedItems = new ArrayList<>();
        for (OrderedItem o : items) {
            Item item = databaseService.getItemById(o.getItemID());
            String name = item.getName();
            double itemprice = item.getRetailprice();
            String comment = o.getComment();
            printableOrderedItems.add(new PrintableOrderedItem(name, itemprice, comment));
            price += itemprice;
        }
        // Preis
        //double price = databaseService.getOrderPrice(order.getOrderID());

        return new PrintableReceipt(dateString, tableName, printableOrderedItems, price);
    }

    private ArrayList<PrintableDataConflict> getPrintableDataConflicts(ArrayList<OrderedItem> orderedItems) {
        ArrayList<PrintableDataConflict> printableDataConflicts = new ArrayList<>();
        String tableName;
        String itemName;
        for (OrderedItem o : orderedItems) {
            tableName = databaseService.getTableById(databaseService.getOrderById(o.getOrderID()).getTable()).getName();
            itemName = databaseService.getItemById(o.getItemID()).getName();
            printableDataConflicts.add(new PrintableDataConflict(tableName, itemName));
        }

        return printableDataConflicts;
    }

    // Formatierung der zu druckenden Daten

    /**
     * Formatiert eine {@link PrintableReceipt} in einen Text, der entsprechend des Layouts des Belegs formattiert wird.
     *
     * @param printableReceipt Daten der Order in ausdruckbarem Format.
     * @return Die Order formatiert in einen Text, der als Beleg ausgedruckt werden kann.
     */
    private String getFormattedReceipt(PrintableReceipt printableReceipt) {
        StringBuilder formattedReceiptText = new StringBuilder("");

        formattedReceiptText.append("----------Kundenbeleg-------------\n\n");

        if(!printableReceipt.getPrintableOrderedItems().isEmpty()) {

            formattedReceiptText
                    .append(Gastronomy.getName()).append("\n")
                    .append(Gastronomy.getAdress()).append("\n")
                    .append(Gastronomy.getTelephonenumber()).append("\n")
                    .append("\n").append("Ihre Bestellung:\n");

            DecimalFormat df = new DecimalFormat("#0.00");
            for (PrintableOrderedItem o : printableReceipt.getPrintableOrderedItems()) {
                if (o.getName().length() >= 25) {
                    formattedReceiptText
                            .append(o.getName())
                            .append("\t")
                            .append(df.format(o.getPrice()))
                            .append(" EUR\n");
                } else if(o.getName().length() <= 7) {
                    formattedReceiptText
                            .append(o.getName())
                            .append("\t\t\t")
                            .append(df.format(o.getPrice()))
                            .append(" EUR\n");
                }else{
                    formattedReceiptText
                            .append(o.getName())
                            .append("\t\t")
                            .append(df.format(o.getPrice()))
                            .append(" EUR\n");
                }

                if (o.getComment() != null)
                    formattedReceiptText.append("\t").append(o.getComment()).append("\n");
            }

            double mwst = Math.round(printableReceipt.getPrice() * 0.199 * 100d) / 100d;

            formattedReceiptText
                    .append("________\n")
                    .append("Summe\t\t").append(df.format(printableReceipt.getPrice())).append(" EUR\n")
                    .append("inkl. MWST 19%\t").append(df.format(mwst)).append(" EUR\n")
                    .append("\n")
                    .append("Sie saßen an Tisch ").append(printableReceipt.getTableName()).append(".\n")
                    .append("Vielen Dank für Ihren Besuch!\n")
                    .append(DateTime.now()).append("\n\n");

            return formattedReceiptText.toString();

        }else{
            throw new IllegalArgumentException("Keine Bestellung vorhanden.");
        }
    }

    /**
     * Formatiert eine {@link PrintableOrder} in einen formatierten Text, der anschließend ausgedruckt werden kann.
     *
     * @param printableOrder PrintableOrder, die die zu formatierenden Daten enthält.
     * @return Einen formatierten Text, der ausgedruckt werden kann.
     */
    private String getFormattedOrder(PrintableOrder printableOrder) {
        StringBuilder formattedOrderText = new StringBuilder("");

        formattedOrderText.append("\t\tKÜCHE\n\n");

        for (PrintableOrderedItem p : printableOrder.getOrderedItems()) {
            formattedOrderText
                    .append(p.getName()).append("\n");
            if (p.getComment() != null)
                formattedOrderText.append("\t").append(p.getComment()).append("\n");
        }

        formattedOrderText
                .append("\n").append("Tisch ").append(printableOrder.getTableName()).append("\n")
                .append(printableOrder.getDate())
                .append("\n\n");

        return formattedOrderText.toString();
    }

    private String getFormattedLogindata(String loginname, String password, Waiter waiter) {
        return "Login-Daten\n"
                + DateTime.now().toString("dd.MM.yyyy kk:mm:ss") + "\n"
                + "Benutzername:\t" + loginname + "\n"
                + "Passwort:\t" + password + "\n";
    }

    private String getFormattedDataConflicts(ArrayList<PrintableDataConflict> printableDataConflicts) {
        StringBuilder txt = new StringBuilder("");

        txt.append("Die folgenden Tische informieren:\n");

        for (PrintableDataConflict p : printableDataConflicts) {
            txt.append(p.getTableName()).append("\t").append(p.getItemName()).append("\n");
        }

        return txt.toString();
    }

    // Druckerfunktionen

    /**
     * Druckt über einen printJob einen String, in dem die Bestellung formatiert wurde.
     *
     * @param text formatierter Text der Bestellung.
     */
    private void printString(String text, byte[] qr) {
        // Printer-Service für den Drucker mit dem printerNamen ermitteln
        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();

        PrintService printService[] = PrintServiceLookup.lookupPrintServices(flavor, pras);
        PrintService service = findPrintService(printerName, printService);

        if (service == null)
            throw new DataException("Der Drucker scheint nicht installiert zu sein!");

        DocPrintJob job = service.createPrintJob();

        try {
            byte[] textBytes = text.getBytes("CP437");
            byte[] commandBytes = {29, 86, 65, 0, 0};

            byte[] bytes = new byte[textBytes.length + commandBytes.length + qr.length];

            System.arraycopy(textBytes, 0, bytes, 0, textBytes.length);
            System.arraycopy(commandBytes, 0, bytes, textBytes.length, commandBytes.length);
            System.arraycopy(qr, 0, bytes, textBytes.length + commandBytes.length, qr.length);

            Doc doc = new SimpleDoc(bytes, flavor, null);

            job.print(doc, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private PrintService findPrintService(String printerName, PrintService[] services) {
        for (PrintService service : services) {
            if (service.getName().equalsIgnoreCase(printerName))
                return service;
        }
        return null;
    }

    public byte[] createQRCode(String ids) {
        String qrCodeData = ids;
        String charset = "UTF-8";
        BitMatrix matrix = null;
        Writer writer = new QRCodeWriter();

        try {
            matrix = writer.encode(new String(qrCodeData.getBytes(charset), charset), BarcodeFormat.QR_CODE, 350, 350);
        } catch (WriterException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String test = matrix.toString();

        byte[] out = test.getBytes();


        return out;
    }

    public static void writeQRCode(int id) {
        QRCodeWriter writer = new QRCodeWriter();
        int width = 256, height = 256;
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB); // create an empty image
        int white = 255 << 16 | 255 << 8 | 255;
        int black = 0;
        String content = Integer.toString(id);
        String path = "..\\QRcodes\\" + content + ".png";
        try {
            BitMatrix bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, width, height);
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    image.setRGB(i, j, bitMatrix.get(i, j) ? black : white); // set pixel one by one
                }
            }

            try {
                ImageIO.write(image, "png", new File(path)); // save QR image to disk
            } catch (java.io.IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } catch (WriterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}