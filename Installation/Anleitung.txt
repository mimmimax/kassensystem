Anleitung Installation:

1. Download zip Ordner (https://bitbucket.org/mimmimax/kassensystem/downloads/)
2. Installation JDK8u191
3. Installation MySQL: NUr Workbench, Notifier und Server ausw�hlen (dazu Benutzerdefinierte/ Custom Installation w�hlen)
    User anlegen: DatabaseService
    Passwort: password
    Host: localhost
    Role: DBManager
4. Angelegte Datenbank �ffnen
5. Datenbank importieren (Schema: kassensystem, Datei: Dump20190810.sql)
6. intelliJ developer edition herunterladen und installieren: https://download.jetbrains.com/idea/ideaIU-2019.2.exe
7. Beide Projekte jeweils �ffnen (File -> Open -> DatabaseSystem bzw ManagerApplication, es muss jeweils der gesamte Ordner gew�hlt werden)
8. Setup SDK (1.8) (zu finden unter File -> Project Structure -> Project)
9. Warten, bis komplett geladen
10. DatabaseSystem: Rebuild Project (Icon: Hammer) -> Run Project (Icon: Play-Button)
11. ManagerApplication: Rebuild Project -> Run Project
